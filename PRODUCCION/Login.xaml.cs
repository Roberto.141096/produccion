﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security.Cryptography;
using System.Globalization;
using System.Text.RegularExpressions;
using System.IO;
using System.Drawing;

namespace PRODUCCION
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        Conexion cn;
        public Login()
        {
            InitializeComponent();
            //txtUser.Focus();
        }      
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            cn = new Conexion("PRODUCCION");
            if (txtUser.Text!=""&&PassContraIngre.Password!="")
            {
                string varpassReg = PassContraIngre.Password;
                MD5 md5 = MD5.Create();
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(varpassReg);
                byte[] hash = md5.ComputeHash(inputBytes);
                varpassReg = BitConverter.ToString(hash).Replace("-", "");
                int i = cn.Login(txtUser.Text, varpassReg);
                if (i == 2)
                {
                    sliding sli = new sliding();
                    this.Hide();
                    sli.Owner = this;
                    sli.ShowDialog();
                    this.Show();
                }
                else if (i == 1)
                {
                    Message message = new Message("Usuario o contraseña incorrecta.");
                    message.ShowDialog();
                }
                else if (i == 3)
                {
                    Message message = new Message("Ha ocurrido un error al conectar al servidor. Error 0x1");
                    message.ShowDialog();
                }
            }
            else
            {
                Message message = new Message("Favor de llenar los campos");
                message.ShowDialog();
            }
        }


        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void PassContraIngre_LostFocus(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(PassContraIngre.Password))
            {
                PassContraIngre.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF999999"));
                PassContraIngre.Password = "Contraseña";
            }
        }

        private void PassContraIngre_GotFocus(object sender, RoutedEventArgs e)
        {
            if (PassContraIngre.Password == "Contraseña")
            {
                PassContraIngre.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF515151"));
                PassContraIngre.Password = "";
            }
        }

        private void txtUser_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtUser.Text == "Usuario")
            {
                txtUser.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF515151"));
                txtUser.Text = "";
            }
        }

        private void txtUser_LostFocus(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtUser.Text))
            {
                txtUser.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF999999"));
                txtUser.Text = "Usuario";
            }
        }
        bool first = false;
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (first == false)
                txtUser.Focus();
            first = true;
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PRODUCCION
{
    /// <summary>
    /// Lógica de interacción para Message.xaml
    /// </summary>
    public partial class Message : Window
    {
        Border fadePOS;
        public bool isAccepted = false;

        int delaySeconds = 3;

        public Message(string constText, Border fade = null, bool isControlsMessage = false)
        {
            InitializeComponent();

            fadePOS = fade;
            txtText.Text = constText;


            if (isControlsMessage)
            {
                OKControl.Visibility = Visibility.Collapsed;
                otherControls.Visibility = Visibility.Visible;
            }
            if (fade != null) fade.Visibility = Visibility.Visible;

        }

        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            if (fadePOS != null) fadePOS.Visibility = Visibility.Hidden;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            if (fadePOS != null) fadePOS.Visibility = Visibility.Hidden;
        }

        private void BtnAccept_Click(object sender, RoutedEventArgs e)
        {
            isAccepted = true;
            this.Close();
            if (fadePOS != null) fadePOS.Visibility = Visibility.Hidden;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Close();
                if (fadePOS != null) fadePOS.Visibility = Visibility.Hidden;
            }
            else if (e.Key == Key.Enter)
            {
                isAccepted = true;
                this.Close();
                if (fadePOS != null) fadePOS.Visibility = Visibility.Hidden;
            }
        }

        private async void TimerClose()
        {
            for (int i = delaySeconds; i > 0; i--)
            {
                txtTimer.Text = "Cerrando en " + i + "s";
                await Task.Delay(1000);
            }
            this.Close();
            if (this.IsLoaded && fadePOS != null) fadePOS.Visibility = Visibility.Hidden;

        }

        private void Window_Activated(object sender, EventArgs e)
        {
            if (OKControl.Visibility != Visibility.Collapsed)
                TimerClose();
        }
    }
}

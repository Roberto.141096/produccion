﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace PRODUCCION
{
    /// <summary>
    /// Lógica de interacción para DashBoard.xaml
    /// </summary>
    public partial class DashBoard : Window
    {
        public DashBoard()
        {
            InitializeComponent();
        }
        Conexion cn;
        public Border fade;
        private void Btnhide_Click(object sender, RoutedEventArgs e)
        {
            menuslide("hidemenu", btnhide, btnshow, leftmenu);
            
        }

        private void menuslide(string v, Button btnhide, Button btnshow, StackPanel leftmenu)
        {
            Storyboard sb = Resources[v] as Storyboard;
            sb.Begin(leftmenu);
            if (v.Contains("showmenu"))
            {
                BrdEle.Background = Brushes.White;
                btnhide.Visibility = System.Windows.Visibility.Visible;
                btnshow.Visibility = System.Windows.Visibility.Hidden;
            }
            else if (v.Contains("hidemenu"))
            {
                BrdEle.Background = null;
                btnhide.Visibility = System.Windows.Visibility.Hidden;
                btnshow.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void Btnshow_Click(object sender, RoutedEventArgs e)
        {
            menuslide("showmenu", btnhide, btnshow, leftmenu);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void BtnVaciada_Click(object sender, RoutedEventArgs e)
        {
            GridVaciada.Visibility = Visibility.Visible;
            DateTime dt = DateTime.Now;
            txtRegistroFecha.Text=dt.ToString();
            cn = new Conexion("EMELSA");
            cn.LoadCbo("SELECT MATERIAL_ID,MATERIAL_DESC FROM TBA_MATERIALES;", ref cboMaterial, "MATERIAL_ID", "MATERIAL_DESC");
            cn.LoadCbo("SELECT PARTEFUN_ID,PARTEFUN_NOMBRE FROM TBA_PARTES_FUNDICION;", ref cboNumPie, "PARTEFUN_ID", "PARTEFUN_ID");
            cn = new Conexion("DB_ERP_EMELSA");
            cn.LoadCbo("SELECT emp_id, (emp_nombre+' '+emp_appat+' '+emp_apmat) AS NOMBRE FROM empleados WHERE emp_id=49 OR emp_id=34;", ref cboEmpleado, "emp_id", "NOMBRE");
            if (cboEmpleado.Items.Count > 0)
                cboEmpleado.SelectedIndex = 0;
            LoadCboVaciadas();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GridVaciada.Visibility = Visibility.Hidden;
        }

        private void CboMaterial_TextInput(object sender, TextCompositionEventArgs e)
        {
            
        }

        private void TxtIdentificador_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.OemPeriod || e.Key == Key.Enter || e.Key == Key.F2 || e.Key==Key.Decimal || txtIdentificador.Text.Length > 2)
                e.Handled = false;
            else if (txtIdentificador.Text.Length > 1)
                e.Handled = true;
            else if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void TxtNumMol_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.OemPeriod || e.Key == Key.Enter || e.Key == Key.F2 || e.Key == Key.Decimal)
                e.Handled = false;
            else if (txtNumMol.Text.Length > 1)
                e.Handled = true;
            else if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
                e.Handled = false;
            else
                e.Handled = true;
        }
        private void LoadCboVaciadas()
        {
            DateTime dt = new DateTime();
            if (DateTime.TryParse(txtRegistroFecha.Text,out dt))
            {
                cn = new Conexion("PRODUCCION");
                cn.LoadCbo("SELECT ID_VACIADA, NUM_VACIADA FROM VACIADA WHERE FECHA = '"+txtRegistroFecha.Text.Replace("'"," ")+"'; ", ref cboNumVac, "ID_VACIADA", "NUM_VACIADA");
                if (cboNumVac.Items.Count > 0)
                    cboNumVac.SelectedIndex = cboNumVac.Items.Count-1;
            }
        }
        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            if (txtRegistroFecha.Text != "" && txtIdentificador.Text != "" && cboMaterial.Text != "" && cboEmpleado.Text != "")
            {
                if (cboMaterial.SelectedValue!=null)
                {
                    if (cboEmpleado.SelectedValue!=null)
                    {
                        string fecha = txtRegistroFecha.Text;
                        string material = cboMaterial.Text;
                        string ident = (txtIdentificador.Text.Length == 1) ? "0" + txtIdentificador.Text : txtIdentificador.Text;
                        string id_empleado = cboEmpleado.SelectedValue.ToString();
                        DateTime dt = new DateTime();
                        if (DateTime.TryParse(fecha, out dt))
                        {
                            ident += dt.ToString("ddMMyy");
                            cn = new Conexion("PRODUCCION");
                            cn.InsertVaciada(ref ident, ref fecha, ref material, ref id_empleado);
                            LoadCboVaciadas();
                        }
                        else
                        {
                            Message message = new Message("Fecha inválida.", fade);
                            message.ShowDialog();
                        }
                    }
                    else
                    {
                        Message message = new Message("Este empleado no se encuentra registrado.", fade);
                        message.ShowDialog();
                    }
                }
                else
                {
                    Message message = new Message("Este material no se encuentra registrado.", fade);
                    message.ShowDialog();
                }

            }
            else
            {
                Message message = new Message("Favor de llenar todos los campos de de la vaciada.", fade);
                message.ShowDialog();
            }


        }

        private void BtnAddPieza_Click(object sender, RoutedEventArgs e)
        {
            if (cboNumPie.Text!=""&&txtNumMol.Text!="")
            {
                if (cboNumPie.SelectedValue!=null)
                {
                    string idPieza = cboNumPie.SelectedValue.ToString();
                    int moldes = Convert.ToInt32(txtNumMol.Text);
                    int idVaciada = Convert.ToInt32(cboNumVac.SelectedValue.ToString());
                    cn = new Conexion("PRODUCCION");
                    cn.InsertVaciadaPieza(ref idVaciada, ref idPieza, ref moldes);
                    LoadDgvPiezas();
                }
                else
                {
                    Message message = new Message("Esta pieza no se encuentra registrada.");
                    message.ShowDialog();
                }
            }
            else
            {
                Message message = new Message("Favor de llenar los campos.");
                message.ShowDialog();
            }
           
        }
        public void LoadDgvPiezas()
        {
            try
            {
                if (cboNumVac.SelectedValue != null)
                {
                    int idVaciada = Convert.ToInt32(cboNumVac.SelectedValue.ToString());
                    cn = new Conexion("PRODUCCION");
                    cn.LoadVaciadaPieza(ref dgvPiezas, ref idVaciada);
                    double kg = 0.0, lb = 0.0,br = 0.0, mo = 0.0, to = 0.0;
                    if (dgvPiezas.Items.Count > 0)
                    {
                        foreach (System.Data.DataRowView dr in dgvPiezas.ItemsSource)
                        {
                            kg += Convert.ToDouble(dr[2].ToString()); lb += Convert.ToDouble(dr[3].ToString()); br += Convert.ToDouble(dr[4].ToString()); mo += Convert.ToDouble(dr[5].ToString()); to += Convert.ToDouble(dr[7].ToString());
                        }
                        txtbKg.Text = kg.ToString();
                        txtbLb.Text = lb.ToString();
                        txtbBr.Text = br.ToString();
                        txtbMo.Text = mo.ToString();
                        txtbTo.Text = to.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                Message message = new Message("Error al sumar los datos. Error 0x6 ", fade);
                message.ShowDialog();
            }
            
        }
        private void CboNumVac_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadDgvPiezas();
        }

        private void TxtRegistroFecha_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime dt = new DateTime();
            if (DateTime.TryParse(txtRegistroFecha.Text, out dt))
                LoadCboVaciadas();
        }
    }
}

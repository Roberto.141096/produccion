﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Windows;
using System.Data;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using System.Windows.Controls;
using System.Windows.Input;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Management;
using System.Diagnostics;

using System.Data.OleDb;
using System.Data.Odbc;
using System.Data.SqlClient;


namespace PRODUCCION
{
    class Conexion
    {
        public Border fade;
        SqlConnection cnMaster;
        
        SqlCommand cmd;
        public Conexion(string BD){
            if (BD=="EMELSA")
                cnMaster = new SqlConnection(ConfigurationManager.ConnectionStrings["EMELSA"].ConnectionString);
            else if(BD=="PRODUCCION")
                cnMaster = new SqlConnection(ConfigurationManager.ConnectionStrings["PRODUCCION"].ConnectionString);
            else if(BD== "DB_ERP_EMELSA")
                cnMaster = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_ERP_EMELSA"].ConnectionString);
        }
        public int Login(string user, string pass)
        {
            try
            {
                cnMaster.Open();
                user.Replace("'", " ");
                using (cmd=new SqlCommand("SELECT COUNT(ID_USUARIO) FROM USUARIO WHERE USUARIO='"+user+"' AND CONTRASENA='"+pass+"'",cnMaster))
                {
                    int i = Convert.ToInt32(cmd.ExecuteScalar());
                    cnMaster.Close();
                    if (i == 0)
                        return 1;
                }
                return 2;
            }
            catch (Exception)
            {
                cnMaster.Close();
                return 3;
            }
        }
        public void LoadCbo(string query,ref ComboBox cboItem,string valueP,string memberP)
        {
            try
            {
                cnMaster.Open();
                using (cmd = new SqlCommand(query, cnMaster))
                {
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    cboItem.SelectedValuePath = valueP;
                    cboItem.DisplayMemberPath = memberP;
                    cboItem.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Roboto Black");
                    cboItem.ItemsSource = dt.AsDataView();


                }
                cnMaster.Close();
            }
            catch
            {
                cnMaster.Close();
                Message message = new Message("Error al cargar componentes. Error 0x2", fade);
                message.ShowDialog();
                

            }
        }
        public void LoadVaciadaPieza(ref DataGrid dgvPiezas,ref int idVaciada)
        {
            try
            {
                cnMaster.Open();
                SqlDataAdapter da;
                using (da = new SqlDataAdapter("SELECT VP.ID_PIEZA,MA.MATERIAL_DESC,PM.MATERIALF_PESO,PM.MATERIALF_PESO_LBS,PM.MATERIALF_PESOBRUTO,VP.NUMERO_MOLDES,M.NUMIMP_MODELO,(VP.NUMERO_MOLDES*M.NUMIMP_MODELO) AS TOTAL FROM  VACIADA_PIEZA AS VP INNER JOIN VACIADA AS V ON(VP.ID_VACIADA = V.ID_VACIADA) INNER JOIN EMELSA.[dbo].TBA_PARTES_FUNDICION AS P ON(VP.ID_PIEZA = P.PARTEFUN_ID) INNER JOIN EMELSA.[dbo].TBA_PFMATERIAL AS PM ON(P.PARTEFUN_ID = PM.PIEZAFUN_ID) INNER JOIN EMELSA.[dbo].TBA_MATERIALES AS MA ON(MA.MATERIAL_ID = PM.MATERIALF_ID) INNER JOIN EMELSA.[dbo].TBA_MODELOSPIEZA AS MO ON(MO.ID_PIEZA = PM.PIEZAFUN_ID) INNER JOIN EMELSA.[dbo].TBA_MODELOS AS M ON(MO.ID_MODELO = M.ID_MODELO) WHERE V.ID_VACIADA = "+idVaciada+" GROUP BY VP.ID_PIEZA, MA.MATERIAL_DESC, PM.MATERIALF_PESO, PM.MATERIALF_PESO_LBS, PM.MATERIALF_PESOBRUTO, VP.NUMERO_MOLDES, M.NUMIMP_MODELO ORDER BY VP.ID_PIEZA;", cnMaster))
                {
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dgvPiezas.ItemsSource = dt.DefaultView;
                    cnMaster.Close();

                    dgvPiezas.Columns[0].Header = "No. Pieza";
                    dgvPiezas.Columns[1].Header = "Material";
                    dgvPiezas.Columns[2].Header = "Peso KG";
                    dgvPiezas.Columns[3].Header = "Peso Lb";
                    dgvPiezas.Columns[4].Header = "Peso Bruto";
                    dgvPiezas.Columns[5].Header = "No. Moldes";
                    dgvPiezas.Columns[6].Header = "Impresiones";
                    dgvPiezas.Columns[7].Header = "Total Piezas";

                    dgvPiezas.Columns[0].Width = 180;
                    dgvPiezas.Columns[1].Width = 300;
                    dgvPiezas.Columns[2].Width = 140;
                    dgvPiezas.Columns[3].Width = 140;
                    dgvPiezas.Columns[4].Width = 140;
                    dgvPiezas.Columns[5].Width = 140;
                    dgvPiezas.Columns[6].Width = 140;
                    dgvPiezas.Columns[6].Width = 140;
                    dgvPiezas.Columns[7].Width = 140;
                }

            }
            catch
            {
                Message message = new Message("Error al cargar las piezas. Error: 0x5", fade);
                message.ShowDialog();
                cnMaster.Close();
            }
        }
        public void InsertVaciadaPieza(ref int idVaciada, ref string idPieza, ref int moldes)
        {
            try
            {
                idPieza.Replace("'", " ");
                cnMaster.Open();
                SqlCommand cmd;
                string consult = "INSERT INTO VACIADA_PIEZA VALUES('" + idPieza + "'," + idVaciada + "," + moldes + ");";
                using (cmd = new SqlCommand(consult, cnMaster))
                {
                    cmd.ExecuteNonQuery();
                    Message message = new Message("Piezas agregadas correctamente.");
                    message.ShowDialog();
                }
                cnMaster.Close();
            }
            catch
            {
                Message message = new Message("Ha ocurrido un error al agregar las piezas. Error 0x4", fade);
                message.ShowDialog();
                cnMaster.Close();
            }
        }
        public void InsertVaciada(ref string numV,ref string fecha,ref string idMat,ref string idEmp)
        {
            try
            {
                numV.Replace("'", " "); fecha.Replace("'", " "); idMat.Replace("'", " "); idEmp.Replace("'", " ");
                cnMaster.Open();
                SqlCommand cmd, cmd1;
                string consult1 = "SELECT COUNT(*) FROM VACIADA WHERE NUM_VACIADA='" + numV + "';";
                string consult = "INSERT INTO VACIADA VALUES('" + numV + "','" + fecha + "','" + idEmp + "','" + idMat + "',null);";
                using (cmd1 = new SqlCommand(consult1, cnMaster))
                {
                    int i;
                    i = Convert.ToInt32(cmd1.ExecuteScalar());
                    if (i == 0)
                    {
                        using (cmd = new SqlCommand(consult, cnMaster))
                        {
                            cmd.ExecuteNonQuery();
                        }
                        Message message = new Message("Vaciada agregada correctamente.");
                        message.ShowDialog();
                    }
                    else
                    {
                        Message message = new Message("Esta vaciada ya ha sido registrada anteriormente.");
                        message.ShowDialog();
                    }
                }

                cnMaster.Close();
            }
            catch
            {
                Message message = new Message("Ha ocurrido un error al agregar la vaciada. Error 0x3", fade);
                message.ShowDialog();
                cnMaster.Close();
            }
        }
    }
}

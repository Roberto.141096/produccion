﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace PRODUCCION
{
    /// <summary>
    /// Lógica de interacción para sliding.xaml
    /// </summary>
    public partial class sliding : Window
    {
        public sliding()
        {
            InitializeComponent();
        }

        private void Btnhide_Click(object sender, RoutedEventArgs e)
        {
            menuslide("hidemenu",btnhide,btnshow,leftmenu);
        }

        private void menuslide(string v, Button btnhide, Button btnshow, StackPanel leftmenu)
        {
            Storyboard sb = Resources[v] as Storyboard;
            sb.Begin(leftmenu);
            if (v.Contains("showmenu"))
            {
                btnhide.Visibility=System.Windows.Visibility.Visible;
                btnshow.Visibility = System.Windows.Visibility.Hidden;
            }
            else if (v.Contains("hidemenu"))
            {
                btnhide.Visibility = System.Windows.Visibility.Hidden;
                btnshow.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void Btnshow_Click(object sender, RoutedEventArgs e)
        {
            menuslide("showmenu", btnhide, btnshow, leftmenu);
        }
    }
}
